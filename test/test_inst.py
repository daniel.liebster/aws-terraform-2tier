#!/usr/bin/env python3

import requests
import json

with open('data.json', 'r') as test_data:
    json_data = json.load(test_data)
with open('../instance_ip_addr.txt', 'r') as instance_ip_addr:
    inst_addr = instance_ip_addr.read().strip()

request = f"http://{inst_addr}:8080/builds"
print(f"Testing:  {request}")

result = requests.post(request, json=json_data)

print(result)
print(result.content)
