#!/bin/bash
#
# Deploys a JSON processing service on AWS
# This terraform config reqires terraform  0.12
# 

terraform init

terraform apply -auto-approve

echo;echo;echo "Please remember to adjust the instance inbound CIDR for SSH once deployment is completed. This step should no longer be needed next revision" ;echo
