1. Please run the included 'deploy_service.sh' script to deploy the build reporting service in AWS us-west-2.

2. The test script test/test_lb.py will run a quick validation against the load balancer ip.

3. The 'yum update' in the aws_instance provisioner can be commented out to save deployment time if needed
