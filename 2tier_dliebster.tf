terraform {
    required_version = "> 0.12.0"
}

provider "aws" {
    region = "us-west-2"
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

data "template_cloudinit_config" "POST-instance" {
  gzip          = true
  base64_encode = true

  part {
    content_type = "text/cloud-config"
    content      = <<EOF
#cloud-config
repo_update: true
repo_upgrade: all

packages:
 - httpd
 - python3
 - python3-pip

write_files:
  - content: |
      ${base64encode(file("${path.module}/assets/build_reporter.py"))}
    encoding: b64
    owner: root:root
    path: /usr/local/bin/build_reporter.py
    permissions: '0750'

  - content: |
      ${base64encode(file("${path.module}/assets/cherrypy.service"))}
    encoding: b64
    owner: root:root
    path: /lib/systemd/system/cherrypy.service
    permissions: '0750'

runcmd:
  - sudo pip3 install cherrypy
  - sudo systemctl daemon-reload
  - sudo systemctl enable cherrypy.service
  - sudo systemctl start cherrypy.service
EOF
  }
}

resource "aws_instance" "POST-handler" {
    ami           = "ami-04590e7389a6e577c"
    instance_type = "t2.micro"
    key_name      = aws_key_pair.POST-handler-key.key_name
    user_data = data.template_cloudinit_config.POST-instance.rendered

    security_groups = [
      "${aws_security_group.allow-instance-in.name}",
      "${aws_security_group.allow-out-all.name}"
    ]

    root_block_device {
      delete_on_termination = "true"
    }
    lifecycle {
      create_before_destroy = "true"
    }

    provisioner "local-exec" {
      command = "echo ${aws_instance.POST-handler.public_ip} > assets/instance_ip_addr.txt"
    }

    tags = {
      Name = "POST-handler-instance"
    }
}

resource "aws_security_group" "allow-instance-in" {
  name        = "allow-instance-in"
  description = "Allow inbound connections"

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["${aws_default_vpc.default.cidr_block}"]
  }

  #Modify cidr_blocks below with source CIDR to allow ssh access to the instance from outside the vpc
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    #cidr_blocks = ["${aws_default_vpc.default.cidr_block}"]
  }

}
resource "aws_security_group" "allow-out-all" {
  name        = "allow-out-all"
  description = "Allow all outbound connections"

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "POST-handler-key" {
  key_name   = "POST-handler-key"
  public_key = file("assets/testing.rsa.pub")
}

resource "aws_elb" "POST-handler-elb" {
  name               = "POST-elb"
  availability_zones = ["us-west-2a", "us-west-2b", "us-west-2c"]

  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 8080
    instance_protocol = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
    target              = "TCP:8080"
    interval            = 30
  }

  instances                   = ["${aws_instance.POST-handler.id}"]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  provisioner "local-exec" {
    command = "echo ${aws_elb.POST-handler-elb.dns_name} > assets/lb_dns_name.txt"
  }

  provisioner "local-exec" {
    command = "echo $(cd test && ./test_lb.py)"
  }

  tags = {
    Name = "POST-handler-elb"
  }
}

